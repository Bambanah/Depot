const projectList = {
  depot: {
    repo_url: "Bambanah/Depot",
    display: "Depot (This site)",
    image_name: "depot.png",
  },
  startpage: {
    repo_url: "Bambanah/startpage",
    display: "Startpage",
    image_name: "startpage.png",
  },
  sentiment_analysis: {
    repo_url: "Bambanah/CAB420-SentimentAnalysis",
    display: "Sentiment Analysis (ML)",
    image_name: "sentiment_analysis.jpg",
  },
};

export default projectList;
